const express = require("express");
const nedb = require("nedb");
const rest = require("express-nedb-rest");
const cors = require("cors");
const app = express();

const datastore = new nedb({
    filename: "mycontactapp.db",
    autoload: true
});

const restAPI = rest();
restAPI.addDatastore('contacts', datastore);

app.use(cors());
app.use('/', restAPI);

app.listen( process.env.PORT|| 3000)