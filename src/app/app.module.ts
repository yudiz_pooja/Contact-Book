import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {  BrowserAnimationsModule} from "@angular/platform-browser/animations";
import 'hammerjs';

import { MatToolbarModule, MatCardModule, MatButtonModule, MatIconModule, MatSelectModule, MatInputModule, MatSnackBarModule } from "@angular/material";
import { ListComponent } from './list/list.component';
import { ContactComponent } from './contact/contact.component'
import { DataService } from './data.service';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    MatInputModule,
    MatSnackBarModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
    // environment.production ? ServiceWorkerModule.register('ngsw-worker.js') : []
   
    

  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
