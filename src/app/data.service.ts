import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: Http) { }
  // public endpoint = "http://localhost:3000"
  public endpoint = "https://dfgdsfgds.herokuapp.com/";
  getList(callback) {
 
    this.http.get(`${this.endpoint}/contacts`).subscribe(res => {
      console.log(res.json());

      callback(res.json())
    })
  }

  get(contactId: string, callback) {
    this.http.get(`${this.endpoint}/contacts/${contactId}`).subscribe(res => {
      callback(res.json());
    })
  }


  save(contact, callback) {
    if (contact._id) {
      this.http.put(`${this.endpoint}/contacts/${contact._id}`, contact).subscribe(res => {
        callback(true);
      })
    } else {
      this.http.post(`${this.endpoint}/contacts`, contact).subscribe(res => {
        callback(true);
      })
    }
  }
}
