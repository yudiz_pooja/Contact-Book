import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private snackBar: MatSnackBar,
) { }
  updateNetworkStatusUI() {
    if (navigator.onLine) {
      (document.querySelector("body") as any).style = "";
    }
    else {
      (document.querySelector("body") as any).style = "filter:grayscale(1)";
    }
  }
  ngOnInit() {
    //check network status
    this.updateNetworkStatusUI();
    window.addEventListener("online", this.updateNetworkStatusUI);
    window.addEventListener("offline", this.updateNetworkStatusUI);
    
    //check installation status
    if ((navigator as any).standalone === false) {

      this.snackBar.open("You can add this to the Home Screen", '', {
        duration: 3000
      });
    }
    if ((navigator as any).standalone == undefined) {
      if (window.matchMedia("(display-mode:browser").matches) {
        // in browser
        window.addEventListener('beforeinstallprompt', event => {
          event.preventDefault();
          const sb = this.snackBar.open("Do You want to install this App?", "Install", {
            duration: 5000
          });
          sb.onAction().subscribe(() => {
            (event as any).prompt();
            (event as any).userChoice.then(result => {
              if (result.outcome == 'dismissed') {
                console.log("Dismissed");
              } else {
                console.log("Installed");

              }
            })
          })
          return false;
        })
      }
    }
  }
}
