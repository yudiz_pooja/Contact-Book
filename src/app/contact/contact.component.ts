import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Contact } from '../logic/contact';
import { DataService } from '../data.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contact: Contact;
  routingSub: any;
  relations = ["Assistant", "Brother", "Child", "Father", "Friend", "Mother", "partner", "sister"];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private data: DataService) { }

  ngOnInit() {
    this.contact = new Contact()
    this.routingSub = this.route.params.subscribe(params => {
      console.log(params['id']);
      if (params["id"]) {
        this.data.get(params['id'], res => {
          this.contact = res;
        })
      }
    });
  }

  cancel() {
    this.router.navigate(['/']);
  }

  save() {
    this.data.save(this.contact, result => {
      if (result) {
        this.router.navigate(['/']);
      }
    })
  }
  
  ngOnDestory() {
    this.routingSub.unsubscribe();
  }
}
















