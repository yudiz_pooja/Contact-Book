
export class Contact {
  _id: string
  phoneno: number
  email: string
  relation: string
  notes: string
  constructor(public name: string = "", public address: string = "") { }
}