import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  {path:'', component:ListComponent},
  {path:'contact',component:ContactComponent},
  {path:'contact/:id',component:ContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
