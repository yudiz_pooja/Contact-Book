import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Contact } from '../logic/contact';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  list: [Contact];
  constructor(private data: DataService, private router: Router) { }

  ngOnInit() {
    this.data.getList(list =>{
      this.list=list;
     } )
  }
  goShare(contact: Contact) {
    const shareText = ` *${contact.name} 's* contact no is  ${contact.phoneno}`;
    if ('share' in navigator) {
      (navigator as any).share({
        title: contact.name,
        text: shareText,
        url: window.location.href
      }).then(() => {
        console.log("shared");

      }).catch(() => {
        console.log("error shared");

      });
    } else {
      const shareURL = `whatsapp://send?text=${encodeURIComponent(shareText)}`
      location.href = shareURL;
    }
  }

  goDetails(contact: Contact) {
    this.router.navigate(["/contact", contact._id])
  }

}
