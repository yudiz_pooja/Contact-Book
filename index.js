const path = require('path');
const express = require('express');
const app = express();
const publicPath = path.join(__dirname, 'public');
const port = process.env.PORT || 3000;

console.log(path.join(publicPath, 'dist', 'index.html'));

app.get('*', (req, res) => {
    res.sendFile(path.join(publicPath, 'dist', 'index.html'));
});

app.listen(port, () => {
    console.log('Server is up!', port);
});